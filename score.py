#!/usr/bin/env python3

from yaml import load
import json
import argparse
import numpy as np
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

components = ['scanner']

def get_weights(config):
    out = dict()
    for component in components:
        out[component] = dict()
        for key in config[component]:
            entry = config[component][key]
            if 'weight_exp' in entry:
                out[component][key] = dict()
                out[component][key]['min'] = entry['weight_exp']['min']
                out[component][key]['max'] = entry['weight_exp']['max']
            else:
                out[component][key] = entry['weight'] if 'weight' in entry else 1

    return out

def get_items_negative_points(item, key, weights, line_count):
    if key == 'comment_amount_exception' and\
            'comments' in weights and\
            isinstance(weights['comments'], dict):
        weight_key = 'comments'
        lines = item['comment_lines']
        weight_min = weights[weight_key]['min']
        weight_max = weights[weight_key]['max']
        percentage_expected = item['expected_percent']
        percentage = lines / line_count
        space = np.logspace(np.log(weight_min), np.log(weight_max),
                            int(0.01 * percentage_expected * line_count) + 1,
                            base=np.exp(1))[::-1]
        return space[lines]
    elif key == 'function_length' and\
            key in weights and\
            isinstance(weights[key], dict):
        weight_min = weights[key]['min']
        weight_max = weights[key]['max']
        if isinstance(item, list):
            expected_line_length = item[0]['expected']
        else:
            expected_line_length = item['expected']
        space = np.logspace(np.log(weight_min), np.log(weight_max),
                            # create the range over double the expected length
                            expected_line_length,
                            base=np.exp(1))
        if isinstance(item, list):
            points = 0
            for function in item:
                extra_length = function['length'] - expected_line_length
                points += (weight_max
                           if extra_length >= expected_line_length
                           else space[extra_length])
            return points
        else:
            extra_length = item['length'] - expected_line_length
            return (weight_max
                    if extra_length >= expected_line_length
                    else space[extra_length])
    else:
        if isinstance(item, list):
            points = 0
            points += len(item) * weights.get(key, 1)
            return points
        else:
            return weights.get(key, 1)

def calculate_score(data, weights, line_count, unfiltered_line_count):
    score = 0
    for key in data:
        score += get_items_negative_points(data[key], key, weights,
                                           unfiltered_line_count)

    score = 10.0 - ((score / line_count) * 10)

    return score


def print_issues(data, filename, line_count, weights):
    line_issues = [[] for i in range(line_count + 1)]
    for key in data:
        if key == 'comment_amount_exception':
            item = data[key]
            issue = f": Low amount of comment lines - {item['comment_lines']} "
            issue += f"({item['found_percent']}/{item['expected_percent']}% of lines)"
            points = get_items_negative_points(item, key, weights, line_count)
            issue += f"[{points:.2f} point(s) lost]"
            line_issues[0].append(issue)
        elif key == 'function_length':
            for item in data[key]:
                issue = f" L{item['line']}: Function \"{item['function']}\" too long "
                issue += f"({item['length']}/{item['expected']})"
                points = get_items_negative_points(item, key, weights, line_count)
                issue += f"[{points:.2f} point(s) lost]"
                line_issues[item['line']].append(issue)
        elif key == 'line_length':
            for item in data[key]:
                issue = f" L{item['line']}: Line too long "
                issue += f"({item['line_length']}/{item['expected']})"
                points = get_items_negative_points(item, key, weights, line_count)
                issue += f"[{points:.2f} point(s) lost]"
                line_issues[item['line']].append(issue)
        elif key == 'indent':
            for item in data[key]:
                issue = f" L{item['line']}: {item['description']} "
                issue += f"(Found: \"{item['found']}\", "
                issue += f"expected: \"{item['expected']}\")"
                issue = issue.replace("\t", "\\t")
                points = get_items_negative_points(item, key, weights, line_count)
                issue += f"[{points:.2f} point(s) lost]"
                line_issues[item['line']].append(issue)
        elif key == 'braces':
            for item in data[key]:
                issue = f" L{item['line']}, C{item['char']}: {item['description']} "
                points = get_items_negative_points(item, key, weights, line_count)
                issue += f"[{points:.2f} point(s) lost]"
                line_issues[item['line']].append(issue)
        elif key == 'operator_spaces':
            for item in data[key]:
                issue = f" L{item['line']}, C{item['char']}: {item['description']} "
                points = get_items_negative_points(item, key, weights, line_count)
                issue += f"[{points:.2f} point(s) lost]"
                line_issues[item['line']].append(issue)

    for line in line_issues:
        for issue in line:
            print(f"{filename}{issue}")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Calculate neatness score')
    parser.add_argument('-c', dest='config_file', default=None,
                        help='The configuration file with weights')
    parser.add_argument('-i', dest='input_file', default='output.yaml',
                        help='The input file with found issues + metadata')
    parser.add_argument('-d', dest='divisor', default='line_count',
                        help='The metadata entry used as the divisor of the' +
                        'score')
    parser.add_argument('-p', dest='prettyprint', default=False,
                        action='store_true',
                        help='Prettyprints the list of issues')
    args = parser.parse_args()

    if args.config_file is None:
        try:
            config_file = open("config.yaml")
            config = load(config_file, Loader=Loader)
        except FileNotFoundError:
            config = None
    else:
        config_file = open(args.config_file)
        config = load(config_file, Loader=Loader)

    data_file = open(args.input_file)
    data = load(data_file, Loader=Loader)
    if config is None:
        weights = {'scanner': dict()}
    else:
        weights = get_weights(config)
    divisor = data['meta'][args.divisor]
    line_count = data['meta']['line_count']
    filename = data['meta']['filename']
    data.pop('meta')
    weights = weights[components[0]]

    if args.prettyprint:
        print_issues(data, filename, line_count, weights)

    score = calculate_score(data, weights, divisor, line_count)
    if args.prettyprint:
        print(f"Your code's neatness score is: {score:.3f}/10")
    else:
        print(score)
